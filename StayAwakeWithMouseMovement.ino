#include "Keyboard.h"
#include "Mouse.h"

// set pin numbers for the five buttons:
const int ledProgrammON = 13;  // red
const int buttonPin = 7;  // the number of the pushbutton pin
const int ledProgrammOff = 11;  //green when nothing is executed
bool systemInitialized = false;
volatile byte interruptionDetected = 0; 
volatile byte runApp = 0;

void setup() {  // initialize the buttons' inputs:
  Mouse.begin();
  Keyboard.begin();
  pinMode(ledProgrammON, OUTPUT);
  pinMode(ledProgrammOff, OUTPUT);
  pinMode(buttonPin, INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(buttonPin), interruptionHandler, FALLING);
  systemInitialized = true;
}

void interruptionHandler() {
  if(systemInitialized) {
    if(interruptionDetected == 0) {
      if (digitalRead(buttonPin) == 0) { //button pressed
        interruptionDetected= 1;
      }
    } 
  }
}

//return true if switched
bool readButton() {
  static long int elapse_timer;
  static bool     switching_pending = false;
  if(interruptionDetected == 1) {
    if (digitalRead(buttonPin) == 0) { //button pressed
      elapse_timer = millis();
      switching_pending = true;
    } 
    if (switching_pending && digitalRead(buttonPin) == 1){
      if((millis() - elapse_timer) >= 10) {//10ms passed
        switching_pending = false;
        return true;
      }
    }
  }
  return false;

}


void loop() {
  bool buttonPressed = readButton();
  if(buttonPressed) {
    if(runApp == 0) {
      runApp = 1;
    }
    else {
      runApp = 0;
    } 

  }

  digitalWrite(ledProgrammOff, !runApp);
  digitalWrite(ledProgrammON, runApp);
        
  if(runApp) {
      Mouse.move(1, 1);
      delay(80);
      Mouse.move(-1,-1);
      delay(80);
  }
}


