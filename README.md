# StayAwakeWithMouseMovement
The code causes the one pixel mouse movement to fake your presence aat your pc. Every monitoring app will thing that you are moving the mouse completing some tasks. You don't need to install any drivers.  

Required parts:
- two 220 Ohm resistors
- green led
- red led
- six goldpins to connect to the Arduino Leonardo board
- Arduino Leonardo board
- micro usb cable for the development board
- any keyboard switch

## Arduino Code
The code causes the mouse movement about one pixel. The movement starts after pressing the button (falling slope). Between mouse movement command there are 80 ms sleeps. To stop the code execution just press and hold the button for a second and release it. The led connected to the port 13 will turn on, when the movement code is being executed.
The led on port 11 informs that the mouse movemnt is OFF. 

